// Main

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[])
{
  double operando_1, operando_2;
  char operacao;

  operando_1 = atof(argv[1]);
  operacao = argv[2][0];
  operando_2 = atof(argv[3]);

  fprintf(stderr,"Operando 1: %lf\t", operando_1);
  fprintf(stderr,"Operando 2: %lf\t", operando_2);
  fprintf(stderr,"Operador: %c\n", operacao);
  fprintf(stderr, "%lf %c %lf\n", operando_1, operacao, operando_2);

  return 0;
}
